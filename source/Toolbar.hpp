#pragma once

#include "Defines.hpp"
#include "imgui.h"

#include <fstream>
#include <array>

class Toolbar
{
public:
	Toolbar() 
	{
		color[0] = 0.0;
		color[1] = 0.0;
		color[2] = 0.0;
		color[3] = 0.0;
	}

	void Process(Tilemap* tilemap)
	{
		ImGui::Begin( "Toolbar" );

		ImGui::Text( "Clear color" );
		ImGui::ColorEdit4("Color", color);
		ImGui::Separator();

		ImGui::NewLine();
		ImGui::Checkbox( "Show lines", &DRAW_LINES );
		ImGui::Checkbox( "Show collision", &DRAW_COLLISION );
		ImGui::Checkbox( "Show console", &CONSOLE_OPEN );
		ImGui::Checkbox( "Show tileset", &TILESET_OPEN );
		ImGui::Separator();

		ImGui::NewLine();
		ImGui::Text( "Tile set:" );
		if ( ImGui::Button( "Load##1" ) ) {
			this->LoadTileset( tilemap );
		} ImGui::SameLine();
		ImGui::InputText( "##text3", tileset, sizeof(tileset) );
		ImGui::Separator();

		ImGui::NewLine();
		ImGui::Text( "Tile map" );
		if ( ImGui::Button( "Save" ) ) {
			this->SaveMap( tilemap );
		} ImGui::SameLine();
		ImGui::InputText( "##text1", saveMap, sizeof(saveMap) );
		if ( ImGui::Button( "Load##2" ) ) {
			this->LoadMap( tilemap );
		} ImGui::SameLine();
		ImGui::InputText( "##text2", loadMap, sizeof(loadMap) );

        ImGui::End();
	}

	bool SaveMap(Tilemap* map)
	{
		bConsole->Print( "Start saving map..." );

		std::fstream file;
		file.open( this->saveMap, std::ios::out );

		if ( file.is_open() ) {
			auto& cells = map->GetCells();
			auto& size = map->GetMapSize();

			std::string line;

			line = std::to_string( size.x ) + " " + std::to_string( size.y ) + "\n";
			file.write( &line[0], line.length() );

			line = map->GetTilesetPath() + "\n";
			file.write( &line[0], line.length() );
			
			for ( int y = 0; y < size.y; ++y ) {
				line = "";
				for ( int x = 0; x < size.x; ++x ) {
					line += std::to_string( cells[y][x].id ) += " ";
				}
				line.erase( line.begin() + line.size() - 1); line += "\n";
				file.write( &line[0], line.length() );
			}

			file.close();
			bConsole->Print( "Map has been saved" );
			return true;
		} else
			bConsole->Print( "File can't be opened." );

		return false;
	}
	bool LoadMap(Tilemap* map)
	{
		bConsole->Print( "Start loading map..." );

		std::fstream file;
		file.open( this->loadMap, std::ios::in );

		if ( file.is_open() ) {

			std::vector<std::vector<Cell>> cells;
			sf::Vector2i size;

			file >> size.x; 
			file >> size.y;
			map->SetMapSize( size.x, size.y );


			std::string tex_path;
			file >> tex_path;

			sf::Texture texture;
			if ( !texture.loadFromFile( tex_path ) )
				bConsole->Print( "Texture can't be loaded." );
			
			int id;
			for ( auto y = 0; y < size.y; ++y ) {
				std::vector<Cell> temp;
				temp.reserve( size.x );
				for ( auto x = 0; x < size.x; ++x ) {
					file >> id;
					Cell cell( sf::Vector2f( TILE_SIZE * x, TILE_SIZE * y ), texture, sf::IntRect(id * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE), id );

					temp.push_back( cell );
				}
				cells.push_back( temp );
			}
			map->SetCells( cells );
			map->ReloadCells();

			file.close();
			bConsole->Print( "Map has been loaded" );
			return true;
		} else
		bConsole->Print( "File can't be opened." );

		return false;
	}
	bool LoadTileset(Tilemap* tilemap)
	{
		bConsole->Print( "Start loading tileset..." );

		sf::Texture texture;
		if ( !texture.loadFromFile( tileset ) )
			bConsole->Print( "Texture can't be loaded." );

		tilemap->SetTileset( texture, tileset );
		tilemap->ReloadCells();

		bTileset->SetTexture( texture );

		return true;
	}

	inline float* GetColor() { return this->color; }
private:
	float color[4];
	char tileset[50] = {};
	char saveMap[50] = {};
	char loadMap[50] = {};

};