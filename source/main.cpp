/*
*	Tile map creator by
*	Untitled_Guys (C) - lemonlary 
*/

#include <imgui.h>
#include <imgui-SFML.h>

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <functional>
#include <cstring>
#include <math.h>

#include "Defines.hpp"
#include "Tilemap.hpp"
#include "Console.hpp"  
#include "Tileset.hpp"
#include "Toolbar.hpp"
#include "Information.hpp"

int main( void )
{
	sf::RenderWindow window( sf::VideoMode( WINDOW_SIZE.x, WINDOW_SIZE.y ), "Tile Map creator", sf::Style::Default );
	window.setFramerateLimit( 60u );
	ImGui::SFML::Init( window );

	//Tile set
	sf::Texture tex;
	std::string path = "data/tileset(2).png";
	tex.loadFromFile( path );

	//Camera
	sf::View view;
	view.reset( sf::FloatRect( 0, 0, WINDOW_SIZE.x, WINDOW_SIZE.y ) );

	const float MOVE = 5.f;
	const float SPEED = 3.5;

	//Tilemap
	Tilemap tilemap( sf::Vector2i(50, 50), path );

	//Toolbars
	Toolbar toolbar;
	Information info;

	bTileset->SetTexture( tex );

	bool open = true;

	sf::Clock deltaClock;
	sf::Vector2f oldMove = sf::Vector2f(0,0);
	while ( window.isOpen() ) {
		sf::Event e;
		while ( window.pollEvent( e ) ) {
			if ( e.type == sf::Event::Closed )
				window.close();
			else if ( e.type == sf::Event::KeyPressed && sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) )
				window.close();

			ImGui::SFML::ProcessEvent(e);
		}
		///////////////////////////////////
		//ImGUI
		ImGui::SFML::Update(window, deltaClock.restart());
			//Toolbars
			toolbar.Process( &tilemap );
			bConsole->Process();
			bTileset->Process();
			info.Process();
		//ImGui::ShowDemoWindow();

		///////////////////////////////////

		sf::Vector2f move;
		if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
			move.x -= MOVE;
		else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
			move.x += MOVE;
		if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
			move.y -= MOVE;
		else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
			move.y += MOVE;
		if ( sf::Keyboard::isKeyPressed( sf::Keyboard::LShift ) ) {
			move.x *= SPEED;
			move.y *= SPEED;
		}


		view.move( move );
		window.setView( view );

		if ( !ImGui::IsWindowFocused( ImGuiFocusedFlags_AnyWindow ) && !ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow) ) {
			if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) ) {
				sf::Vector2i mouse = sf::Mouse::getPosition( window );
				sf::Vector2f pos = window.mapPixelToCoords( mouse );
				tilemap.SetCell( pos, SELECTED_TILE );
			}
			/*if ( sf::Mouse::isButtonPressed( sf::Mouse::Right ) ) {
				sf::Vector2i mouse = sf::Mouse::getPosition( window );
				sf::Vector2f pos = window.mapPixelToCoords( mouse );

			}*/
		}
		/////////////////////////////////

		float* color = toolbar.GetColor();
		sf::Color clear( color[0] * 255, color[1] * 255, color[2] * 255, color[3] * 255 );

		window.clear( clear );
		tilemap.Draw( window, sf::RenderStates::Default );
		ImGui::SFML::Render( window );
		window.display();
	}

    ImGui::SFML::Shutdown();
	return 0;
}