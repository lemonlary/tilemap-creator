#pragma once

#include <imgui.h>
#include <SFML/Graphics/Texture.hpp>
#include <vector>

#include "Defines.hpp"

class Tileset
{
public:
	Tileset() { }
	Tileset(const sf::Texture& texture) { 
		this->texture = texture;
		this->sprite.setTexture( this->texture );
		this->sprite.setScale( sf::Vector2f( 0.5f, 0.5f ) );

		int size = ( texture.getSize().x / TILE_SIZE );
		this->rects.reserve( size );
		for ( int i = 0; i < size; ++i ) {
			this->rects.push_back( sf::IntRect( i * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE ) );
		}
	}

	void Process()
	{
		if ( TILESET_OPEN ) {
			ImGui::Begin( "Tileset" );
				
			ImGui::BeginChild( "tiles", ImVec2( 0, 64 ), false, ImGuiWindowFlags_HorizontalScrollbar );
			for ( int i = 0; i < this->rects.size(); ++i ) {
				sprite.setTextureRect( rects[i] );
				ImGui::SameLine();
				ImGui::PushID( ("tile##" + std::to_string( i )).c_str() );
				if ( ImGui::ImageButton( sprite, -1 ) ) {
					SELECTED_TILE = i;
				}
				ImGui::PopID();
			}
			ImGui::EndChild();


			ImGui::NewLine();
			ImGui::Text( "Selected tile: %d", SELECTED_TILE );

			ImGui::End();
		}
	}

	void SetTexture( const sf::Texture& tex )
	{
		this->texture = tex;

		this->sprite.setTexture( this->texture );
		this->sprite.setScale( sf::Vector2f( 0.5f, 0.5f ) );

		int size = ( texture.getSize().x / TILE_SIZE );
		this->rects.clear();
		this->rects.reserve( size );
		for ( int i = 0; i < size; ++i ) {
			this->rects.push_back( sf::IntRect( i * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE ) );
		}
	}
private:
	sf::Texture texture;
	sf::Sprite sprite;
	std::vector<sf::IntRect> rects;

};

std::unique_ptr<Tileset> bTileset = std::make_unique<Tileset>();